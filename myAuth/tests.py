from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.contrib.auth.models import User
from myAuth.views import *
from myAuth.forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class MyAuthUnitTest(TestCase):
	def test_login_url_is_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_blank_url_redirect(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 302)

	def test_home_url_is_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 302)

	def test_login_using_login_func(self):
		found = resolve('/login/')
		self.assertEqual(found.func, login)

	def test_login_using_login_template(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'login.html')
		self.assertContains(response, 'Username')
		self.assertContains(response, 'Password')

	def test_login_form_has_placeholder(self):
		form = LoginForm()
		self.assertIn('id="id_username', form.as_p())
		self.assertIn('id="id_password', form.as_p())

	def test_login_form_validation_for_blank_items(self):
		form = LoginForm(data={'username':'', 'password':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['username'], ["This field is required."])
		self.assertEqual(form.errors['password'], ["This field is required."])

	def test_login(self):
		c = Client()
		user = User.objects.create(username='michael')
		user.set_password('michael')
		user.save()
		data = {'username':'michael', 'password':'michael'}

		response = c.post('/login/', data=data)

		response2 = c.get('/login/')
		self.assertEqual(response2.status_code, 302)

		response3 = c.get('/home/')
		self.assertEqual(response3.status_code, 200)

		self.assertIn(user.username, response3.content.decode('utf8'))

		# test blank url after login
		response = c.get('/')
		self.assertEqual(response.status_code, 302)

		# test if book form has placeholder and no values (import from story8 ajax)
		form = BookForm(data={'searchBox':''})
		self.assertIn('id="id_searchBox', form.as_p())		
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['searchBox'], ["This field is required."])

	def test_login_fails(self):
		c = Client()
		user = User.objects.create(username='michael')
		user.set_password('michael')
		user.save()
		data = {'username':'michael', 'password':'susanto'}

		response = c.post('/login/', data=data)
		self.assertIn('Invalid username / password!', response.content.decode('utf8'))

	def test_logout(self):
		c = Client()
		user = User.objects.create(username='michael')
		user.set_password('michael')
		user.save()
		data = {'username':'michael', 'password':'michael'}

		response = c.post('/login/', data=data)
		self.assertEqual(response.status_code, 302)
		response = c.get('/logout/')
		self.assertEqual(response.status_code, 302)

# class MyAuthFunctionalTest(LiveServerTestCase):

# 	# set up selenium
# 	def setUp(self):
# 		super().setUp()
# 		chrome_options = webdriver.ChromeOptions()
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--window-size=1420,1080')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('--disable-gpu')
# 		self.selenium = webdriver.Chrome(chrome_options=chrome_options)

# 	# close selenium
# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(MyAuthFunctionalTest, self).tearDown()

# 	# main flow of functional test
# 	def test_input_todo(self):

# 		# open selenium
# 		selenium = self.selenium

# 		# open localhost
# 		selenium.get('http://127.0.0.1:8000/')

# 		user = User.objects.create(username='michael')
# 		user.set_password('michael')
# 		user.save()

# 		# find the form element
# 		username = selenium.find_element_by_id('id_username')
# 		password = selenium.find_element_by_id('id_password')
# 		login = selenium.find_element_by_id('loginBtn')

# 		# fill form with data
# 		username.send_keys('michael')
# 		password.send_keys('michael')

# 		# submit form
# 		login.send_keys(Keys.RETURN)

# 		self.assertIn(user.username, self.selenium.page_source)