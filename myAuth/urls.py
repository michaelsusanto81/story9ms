from django.urls import path
from . import views

app_name = "myAuth"

urlpatterns = [
	path('', views.blank, name='blank'),
    path('login/', views.login, name='login'),
    path('home/', views.index, name='home'),
    path('logout/', views.logout, name='logout'),
]