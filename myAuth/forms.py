from django import forms

class LoginForm(forms.Form):
	username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'class': 'boxField'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'boxField'}))

class BookForm(forms.Form):
	searchBox = forms.CharField(label='', widget=forms.TextInput(attrs={'class': 'textinput'}))