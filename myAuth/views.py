from django.shortcuts import render, redirect
from .forms import *
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as user_login, logout as user_logout

# Create your views here.
def blank(request):
	user = User.objects.create_superuser(username='dummy', password='dummy', email='dummy@example.com')
	user.set_password('dummy')
	user.save()
	if request.user.is_authenticated:
		return redirect('/home/')
	return redirect('/login/')
	
def index(request):
	if not request.user.is_authenticated:
		return redirect('/login/')
	response = {}
	response['user'] = request.user
	response['bookForm'] = BookForm(request.POST or None)
	return render(request, 'home.html', response)

def login(request):
	response = {}
	form = LoginForm(request.POST or None)
	response['inputForm'] = form
	if request.user.is_authenticated:
		return redirect('/home/')
	else:
		if request.method == "POST":
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(request, username=username, password=password)
			if user is not None:
				user_login(request, user)
				return redirect('/home/')
			else:
				response['error'] = 'Invalid username / password!'
				return render(request, 'login.html', response)
	return render(request, 'login.html', response)

def logout(request):
	user_logout(request)
	return redirect('/login/')